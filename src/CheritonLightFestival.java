import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author Matthew Boakes (mjb228)
 */
public class CheritonLightFestival extends Application {

    // Program Constants.
    private static final String USER_DIR = "user.dir";
    private static final String WORKING_DIR = System.getProperty(USER_DIR);
    private static final String DATA = "/data/";
    private static final String VIDEOS = "videos/";
    private static final String MP4 = ".mp4";
    private static final String PRESS_HERE = "Press-Here.png";
    private static final String PRESS_HERE_IMAGE_PATH = WORKING_DIR + DATA + PRESS_HERE;
    private static final String FRUIT_MACHINE = "Fruit-Machine.mp4";
    private static final String FRUIT_MACHINE_IMAGE_PATH = WORKING_DIR + DATA + FRUIT_MACHINE;
    private static final String CHERITON_LIGHT_FESTIVAL = "Cheriton Light Festival";

    // Program Fields.
    private static GpioPinDigitalInput pressureMat;
    private Random random;
    private boolean canTriggerEvent;
    private Scene imageScene;
    private Scene mediaScene;
    private List<String> planetVideos;
    private MediaPlayer fruitMachine;

    /**
     * The Main method to start the application.
     *
     * @param args Arguments supplied to main.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * First method called to setup the JavaFX Application to display the image and play the videos.
     *
     * @param stage The stage for this application, onto which the application scene can be set.
     * @throws IOException When the video files cannot be found.
     */
    @Override
    public void start(Stage stage) throws IOException {
        setUpFields();
        setUpGpio();
        ImageView imageView = getPressHereImageView();
        planetVideos = getPlanetVideos();
        MediaView mediaView = new MediaView();
        mediaView.setPreserveRatio(true);
        imageScene = getScene(imageView);
        mediaScene = getScene(mediaView);
        setTheStage(stage);
        setEventHandlers(stage, mediaView);
    }

    /**
     * Initialise the fields.
     */
    private void setUpFields() {
        random = new Random();
        canTriggerEvent = true;
        fruitMachine = getMediaPlayer(FRUIT_MACHINE_IMAGE_PATH);
    }

    /**
     * Configure the Raspberry Pi's GPIO to control the pressure mat.
     */
    private static void setUpGpio() {
        GpioController gpioController = GpioFactory.getInstance();
        pressureMat = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_07);
        pressureMat.setShutdownOptions(true);
    }

    /**
     * Get the image that indicates the pressure mat is waiting for input.
     *
     * @return The ImageView containing the 'press here' image.
     */
    private ImageView getPressHereImageView() {
        File pressHereImage = new File(PRESS_HERE_IMAGE_PATH);
        Image pressHere = new Image(pressHereImage.toURI().toString());
        ImageView imageView = new ImageView();
        imageView.setImage(pressHere);
        return imageView;
    }

    /**
     * Get the list of planet videos from the folder. This makes it easier to add or remove videos as required.
     *
     * @return List containing the path to all the planet video files.
     * @throws IOException When the path to the video files cannot be found.
     */
    private List<String> getPlanetVideos() throws IOException {
        return Files.walk(Paths.get(WORKING_DIR + DATA + VIDEOS))
                .filter(this::isVideoFile)
                .map(Path::toString)
                .collect(Collectors.toList());
    }

    /**
     * Checks whether the file is a valid video file (.mp4).
     *
     * @param filePath The path to the video file.
     * @return True if a .mp4 video file.
     */
    private boolean isVideoFile(Path filePath) {
        return filePath.toString().endsWith(MP4);
    }

    /**
     * Creates a scene.
     *
     * @param view the view to be used for the scene (Image or Media).
     * @return The complete scene.
     */
    private Scene getScene(Node view) {
        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(view);
        Scene scene = new Scene(stackPane);
        scene.setFill(Color.BLACK);
        return scene;
    }

    /**
     * Setup the stage.
     *
     * @param stage The stage tobe setup.
     */
    private void setTheStage(Stage stage) {
        stage.setTitle(CHERITON_LIGHT_FESTIVAL);
        stage.setFullScreenExitHint("");
        changeScene(stage, imageScene);
        stage.show();
    }

    /**
     * Change the stage to a different scene (Image or Media).
     *
     * @param stage The stage for this application, onto which the application scene can be set.
     * @param scene The scene to set onto the stage.
     */
    private void changeScene(Stage stage, Scene scene) {
        stage.setScene(scene);
        stage.setFullScreen(true);
    }

    /**
     * Setup all event handlers, mostly for the pressure mat but also for mouse clicks for testing.
     *
     * @param stage     The stage for this application, onto which the application scene can be set.
     * @param mediaView The MediaView that will be used to play and display the videos.
     */
    private void setEventHandlers(Stage stage, MediaView mediaView) {
        imageScene.setOnMouseClicked(event -> playRandom(mediaView, stage));
        mediaScene.setOnMouseClicked(event -> playRandom(mediaView, stage));

        pressureMat.addListener((GpioPinListenerDigital) event -> {
            if (event.getEdge() == PinEdge.FALLING) {
                Platform.runLater(() -> playRandom(mediaView, stage));
            }
        });
    }

    /**
     * Play a random planet video.
     *
     * @param mediaView The MediaView that will be used to play and display the videos.
     * @param stage     The stage for this application, onto which the application scene can be set.
     */
    private void playRandom(MediaView mediaView, Stage stage) {
        if (canTriggerEvent) {
            canTriggerEvent = false;
            changeScene(stage, mediaScene);
            stopCurrentlyPlayingVideo(mediaView);
            MediaPlayer randomPlanet = getRandomPlanetVideo();
            setMediaPlayerEvents(randomPlanet, stage);
            playFruitMachine(mediaView, randomPlanet);
        }
    }

    /**
     * Play the fruit machine (plays looping video followed by randomly selected video).
     *
     * @param mediaView    The MediaView that will be used to play and display the videos.
     * @param randomPlanet The randomly selected planet to play once looping video has finished.
     */
    private void playFruitMachine(MediaView mediaView, MediaPlayer randomPlanet) {
        fruitMachine.setOnEndOfMedia(() -> {
            mediaView.setMediaPlayer(randomPlanet);
            randomPlanet.play();
        });
        mediaView.setMediaPlayer(fruitMachine);
        fruitMachine.seek(Duration.millis(0));
        fruitMachine.play();
    }

    /**
     * Stops playing current video.
     *
     * @param mediaView The MediaView that will be used to play and display the videos.
     */
    private void stopCurrentlyPlayingVideo(MediaView mediaView) {
        MediaPlayer currentMediaPlayer = mediaView.getMediaPlayer();
        if (videoIsPlaying(currentMediaPlayer)) {
            currentMediaPlayer.stop();
            currentMediaPlayer.dispose();
        }
    }

    /**
     * Checks if there is a video playing.
     *
     * @param currentMediaPlayer The currently playing MediaPlayer.
     * @return True if there is a video playing.
     */
    private boolean videoIsPlaying(MediaPlayer currentMediaPlayer) {
        return currentMediaPlayer != null;
    }

    /**
     * Returns a MediaPlayer containing a random planet video.
     *
     * @return Random MediaPlayer with planet video.
     */
    private MediaPlayer getRandomPlanetVideo() {
        int randomIndex = random.nextInt(planetVideos.size());
        return getMediaPlayer(planetVideos.get(randomIndex));
    }

    /**
     * Get a MediaPlayer to contain a planet video.
     *
     * @param mediaPath The path to the planet video.
     * @return MediaPlayer with planet video.
     */
    private MediaPlayer getMediaPlayer(String mediaPath) {
        File videoFile = new File(mediaPath);
        Media media = new Media(videoFile.toURI().toString());
        return new MediaPlayer(media);
    }

    /**
     * Set events for the MediaPlayer to determine what to do once planet video has finished playing.
     *
     * @param mediaPlayer The MediaPlayer containing a planet video.
     * @param stage       The stage for this application, onto which the application scene can be set.
     */
    private void setMediaPlayerEvents(MediaPlayer mediaPlayer, Stage stage) {
        mediaPlayer.setOnStopped(() -> showPressHereImage(mediaPlayer, stage));
        mediaPlayer.setOnEndOfMedia(() -> showPressHereImage(mediaPlayer, stage));
    }

    /**
     * Display the 'press here' image indicating the pressure mat is waiting for input.
     *
     * @param currentMediaPlayer The currently playing MediaPlayer.
     * @param stage              The stage for this application, onto which the application scene can be set.
     */
    private void showPressHereImage(MediaPlayer currentMediaPlayer, Stage stage) {
        currentMediaPlayer.dispose();
        changeScene(stage, imageScene);
        canTriggerEvent = true;
    }

}
