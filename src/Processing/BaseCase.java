package Processing;

import gohai.glvideo.GLMovie;
import processing.core.PApplet;
import processing.io.GPIO;

public class BaseCase extends PApplet {

    private GLMovie video;

    public static void main(String[] args) {
        String[] processingArgs = {"Processing.BaseCase"};
        BaseCase baseCase = new BaseCase();
        PApplet.runSketch(processingArgs, baseCase);
    }

    public void settings() {
        //size(560, 406, P2D);
        fullScreen(P2D);
    }

    public void setup() {
        GPIO.pinMode(4, GPIO.INPUT);
        video = new GLMovie(this, "Cheriton-Light-Festival.mp4");
        video.loop();
    }

    public void draw() {
        background(0);
        if (video.available()) {
            video.read();
        }
        image(video, 0, 0, width, height);
        if (GPIO.digitalRead(4) == GPIO.LOW) {
            video.loop();
        } else {
            video.pause();
        }
    }

}
