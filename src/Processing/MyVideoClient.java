package Processing;

import gohai.glvideo.GLMovie;
import processing.core.PApplet;
import processing.io.GPIO;
import processing.net.Client;

import java.lang.reflect.Field;

/**
 * @author Matthew Boakes (mjb228)
 */
public class MyVideoClient extends PApplet {

    private static final String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 5204;
    private static final int PRESSURE_MAT_PIN = 17;
    private static final String ALL = "All";
    private static final String CLIENT_ID = "Client1";
    private static final String REQUEST = "REQUEST";
    private static final String MATCH = "Match";
    private static final String NO_MATCH = "No Match";
    private static final String SEPARATOR = "-";
    private static final String DEFAULT_VIDEO = "Planet (1).mp4";

    private Client myClient;
    private GLMovie currentMovie;
    private boolean match = false;

    public static void main(String[] args) {
        setLibraryPath();
        String[] processingArgs = {"Processing.MyVideoClient"};
        MyVideoClient myVideoClient = new MyVideoClient();
        PApplet.runSketch(processingArgs, myVideoClient);
    }

    private static void setLibraryPath() {
        // Hack!
        System.setProperty("java.library.path", "./lib/linux-armv6hf");
        Field fieldSysPath;
        try {
            fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void settings() {
        size(560, 406, P3D);
        //fullScreen(P3D);
    }

    public void setup() {
        myClient = new Client(this, SERVER_IP, SERVER_PORT);
        GPIO.pinMode(PRESSURE_MAT_PIN, GPIO.INPUT);
        GPIO.attachInterrupt(PRESSURE_MAT_PIN, this, "pressureMatEvent", GPIO.FALLING);
        frameRate(30);
        currentMovie = new GLMovie(this, DEFAULT_VIDEO);
    }

    public void draw() {
        if (currentMovie.available()) {
            currentMovie.read();
        }
        image(currentMovie, 0, 0, width, height);
        checkServerMessage();
        if (match) {
            displayMatch();
        }
    }

    private void checkServerMessage() {
        if (messageFromServer()) {
            String whatServerSaid = myClient.readString();
            if (whatServerSaid != null) {
                String[] split = whatServerSaid.split(SEPARATOR);
                String clientId = split[0];
                String message = split[1];
                if (messageForThisClient(clientId)) {
                    playCurrentMovie(message);
                } else if (messageForAllClients(clientId)) {
                    checkMatch(message);
                }
            }
        }
    }

    private boolean messageFromServer() {
        return myClient.available() > 0;
    }

    private boolean messageForThisClient(String clientId) {
        return clientId.equals(CLIENT_ID);
    }

    private void playCurrentMovie(String movieName) {
        currentMovie.dispose();
        currentMovie = new GLMovie(this, movieName);
        currentMovie.jump(0);
        currentMovie.play();
    }

    private boolean messageForAllClients(String clientId) {
        return clientId.equals(ALL);
    }

    private void checkMatch(String message) {
        if (message.equals(MATCH)) {
            match = true;
        } else if (message.equals(NO_MATCH)) {
            match = false;
        }
    }

    private void displayMatch() {
        textSize(32);
        text(MATCH, 10, 30);
        fill(0, 102, 153);
    }

    public void mouseClicked() {
        myClient.write(CLIENT_ID + SEPARATOR + REQUEST);
    }

    public void pressureMatEvent(int pin) {
        myClient.write(CLIENT_ID + SEPARATOR + REQUEST);
    }

}
