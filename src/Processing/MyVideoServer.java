package Processing;

import gohai.glvideo.GLMovie;
import processing.core.PApplet;
import processing.io.GPIO;
import processing.net.Client;
import processing.net.Server;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Matthew Boakes (mjb228)
 */
public class MyVideoServer extends PApplet {

    private static final int SERVER_PORT = 5204;
    private static final int PRESSURE_MAT_PIN = 17;
    private static final String VIDEOS_DIRECTORY = "videos/";
    private static final String MP4 = ".mp4";
    private static final String REQUEST = "REQUEST";
    private static final String SEPARATOR = "-";
    private static final String ALL = "All";
    private static final String SERVER = "Server";
    private static final String CLIENT_1 = "Client1";
    private static final String CLIENT_2 = "Client2";
    private static final String MATCH = "Match";
    private static final String NO_MATCH = "No Match";
    private static final String DEFAULT_VIDEO = "Planet (1).mp4";
    private static final String ID_NOT_RECOGNISED = "ID Not Recognised: ";

    private Server myServer;
    private List<String> movieNames = new ArrayList<>();
    private GLMovie currentMovie;
    private String[] currentlyPlaying = {SERVER, CLIENT_1, CLIENT_2};
    private boolean matchSent = false;

    public static void main(String[] args) {
        setLibraryPath();
        String[] processingArgs = {"Processing.MyVideoServer"};
        MyVideoServer myVideoServer = new MyVideoServer();
        PApplet.runSketch(processingArgs, myVideoServer);
    }

    private static void setLibraryPath() {
        // Hack!
        System.setProperty("java.library.path", "./lib/linux-armv6hf");
        Field fieldSysPath;
        try {
            fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void settings() {
        size(560, 406, P3D);
        //fullScreen(P3D);
    }

    public void setup() {
        myServer = new Server(this, SERVER_PORT);
        GPIO.pinMode(PRESSURE_MAT_PIN, GPIO.INPUT);
        GPIO.attachInterrupt(PRESSURE_MAT_PIN, this, "pressureMatEvent", GPIO.FALLING);
        getMovieFileNames();
        frameRate(30);
        currentMovie = new GLMovie(this, VIDEOS_DIRECTORY + DEFAULT_VIDEO);
    }

    public void draw() {
        if (currentMovie.available()) {
            currentMovie.read();
        }
        image(currentMovie, 0, 0, width, height);
        checkClientMessage();
        checkVideoMatch();
    }

    private void getMovieFileNames() {
        File[] listOfFiles = new File(dataPath(VIDEOS_DIRECTORY)).listFiles();
        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.getName().endsWith(MP4)) {
                    movieNames.add(file.getName());
                }
            }
        }
    }

    private void checkClientMessage() {
        Client thisClient = myServer.available();
        if (clientAvailable(thisClient)) {
            String whatClientSaid = thisClient.readString();
            if (messageFromClient(whatClientSaid)) {
                String[] split = whatClientSaid.split(SEPARATOR);
                String clientId = split[0];
                String command = split[1];
                if (command.equals(REQUEST)) {
                    String randomMovie = selectedRandomMovie();
                    myServer.write(clientId + SEPARATOR + randomMovie);
                    updateCurrentlyPlaying(clientId, randomMovie);
                }
            }
        }
    }

    private boolean clientAvailable(Client thisClient) {
        return thisClient != null;
    }

    private boolean messageFromClient(String whatClientSaid) {
        return whatClientSaid != null;
    }

    private String selectedRandomMovie() {
        if (movieNames.size() > 0) {
            int randomMovie = (int) random(movieNames.size());
            return movieNames.get(randomMovie);
        }
        return DEFAULT_VIDEO;
    }

    private void updateCurrentlyPlaying(String id, String movie) {
        switch (id) {
            case SERVER:
                currentlyPlaying[0] = movie;
                break;
            case CLIENT_1:
                currentlyPlaying[1] = movie;
                break;
            case CLIENT_2:
                currentlyPlaying[2] = movie;
            default:
                println(ID_NOT_RECOGNISED + id);
        }
    }

    private void checkVideoMatch() {
        boolean allMatch = Arrays.stream(currentlyPlaying)
                .collect(Collectors.toSet())
                .size() == 1;
        if (allMatch) {
            displayMatch();
            if (!matchSent) {
                myServer.write(ALL + SEPARATOR + MATCH);
                matchSent = true;
            }
        } else {
            if (matchSent) {
                myServer.write(ALL + SEPARATOR + NO_MATCH);
                matchSent = false;
            }
        }
    }

    private void displayMatch() {
        textSize(32);
        text(MATCH, 10, 30);
        fill(0, 102, 153);
    }

    public void mouseClicked() {
        String randomMovie = selectedRandomMovie();
        playCurrentMovie(randomMovie);
        updateCurrentlyPlaying(SERVER, randomMovie);
    }

    public void pressureMatEvent(int pin) {
        String randomMovie = selectedRandomMovie();
        playCurrentMovie(randomMovie);
        updateCurrentlyPlaying(SERVER, randomMovie);
    }

    private void playCurrentMovie(String movieName) {
        currentMovie.dispose();
        currentMovie = new GLMovie(this, VIDEOS_DIRECTORY + movieName);
        currentMovie.jump(0);
        currentMovie.play();
    }

}
