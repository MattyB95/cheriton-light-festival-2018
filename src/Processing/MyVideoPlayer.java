package Processing;

import gohai.glvideo.GLMovie;
import processing.core.PApplet;
import processing.io.GPIO;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Matthew Boakes (mjb228)
 */
public class MyVideoPlayer extends PApplet {

    private static final int PRESSURE_MAT_PIN = 17;
    private static final String VIDEOS_DIRECTORY = "videos/";
    private static final String MP4 = ".mp4";
    private static final String DEFAULT_VIDEO = "Planet (1).mp4";

    private List<String> movieNames = new ArrayList<>();
    private GLMovie currentMovie;

    public static void main(String[] args) {
        setLibraryPath();
        String[] processingArgs = {"Processing.MyVideoPlayer"};
        MyVideoPlayer myVideoPlayer = new MyVideoPlayer();
        PApplet.runSketch(processingArgs, myVideoPlayer);
    }

    private static void setLibraryPath() {
        // Hack!
        System.setProperty("java.library.path", "./lib/linux-armv6hf");
        Field fieldSysPath;
        try {
            fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void settings() {
        size(560, 406, P3D);
        //fullScreen(P3D);
    }

    public void setup() {
        GPIO.pinMode(PRESSURE_MAT_PIN, GPIO.INPUT);
        GPIO.attachInterrupt(PRESSURE_MAT_PIN, this, "pressureMatEvent", GPIO.FALLING);
        getMovieFileNames();
        frameRate(30);
        currentMovie = new GLMovie(this, VIDEOS_DIRECTORY + DEFAULT_VIDEO);
    }

    public void draw() {
        if (currentMovie.available()) {
            currentMovie.read();
        }
        image(currentMovie, 0, 0, width, height);
    }

    private void getMovieFileNames() {
        File[] listOfFiles = new File(dataPath(VIDEOS_DIRECTORY)).listFiles();
        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.getName().endsWith(MP4)) {
                    movieNames.add(file.getName());
                }
            }
        }
    }

    public void mouseClicked() {
        playRandomMovie();
    }

    public void pressureMatEvent(int pin) {
        playRandomMovie();
    }

    private void playRandomMovie() {
        if (movieNames.size() > 0) {
            int randomIndex = (int) random(movieNames.size());
            String randomMovie = movieNames.get(randomIndex);
            playCurrentMovie(randomMovie);
        }
    }

    private void playCurrentMovie(String movieName) {
        currentMovie.dispose();
        currentMovie = new GLMovie(this, VIDEOS_DIRECTORY + movieName);
        currentMovie.jump(0);
        currentMovie.play();
    }

}
