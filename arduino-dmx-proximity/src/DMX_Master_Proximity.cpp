// Author: Matthew Boakes (mjb228)

#include <Arduino.h>
#include <Conceptinetics.h>

// <--------- DMX Configuration --------->

// The master will control 3 Channels (1-3)
const uint16_t DMX_MASTER_CHANNELS = 3;
// Pin number to change read or write mode on the shield
const int RXEN_PIN = 2;

const uint16_t RED_CHANNEL = 1;
const uint16_t GREEN_CHANNEL = 2;
const uint16_t BLUE_CHANNEL = 3;

const uint8_t MIN_BRIGHTNESS = 0;
const uint8_t MAX_BRIGHTNESS = 255;

// Configure a DMX master controller
DMX_Master dmxMaster(DMX_MASTER_CHANNELS, RXEN_PIN);

// <--------- LED Configuration --------->

const uint8_t RED_LED = 2;
const uint8_t GREEN_LED = 3;

// <--------- Ultrasonic Sensor Configuration --------->

// defines ultrasonic pins numbers
const uint8_t TRIG_PIN_ULTRA_ONE = 9;
const uint8_t ECHO_PIN_ULTRA_ONE = 10;

const uint8_t TRIG_PIN_ULTRA_TWO = 11;
const uint8_t ECHO_PIN_ULTRA_TWO = 12;

// defines ultrasonic variables
const int DISTANCE_TRIGGER = 200;
long duration;
double distanceUltraOne;
double distanceUltraTwo;

void setLED(uint8_t redLEDValue, uint8_t greenLEDValue) {
    digitalWrite(RED_LED, redLEDValue);
    digitalWrite(GREEN_LED, greenLEDValue);
}

void setRGB(uint8_t redIntensity, uint8_t greenIntensity, uint8_t blueIntensity) {
    dmxMaster.setChannelValue(RED_CHANNEL, redIntensity);
    dmxMaster.setChannelValue(GREEN_CHANNEL, greenIntensity);
    dmxMaster.setChannelValue(BLUE_CHANNEL, blueIntensity);
}

void redOn() {
    // Turn on red LED
    setLED(HIGH, LOW);
}

void greenOn() {
    // Turn on green LED
    setLED(LOW, HIGH);
}

double sensorSense(uint8_t trigPin, uint8_t echoPin) {
    // Clears the TRIG_PIN
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);

    // Sets the TRIG_PIN on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);

    // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
    duration = pulseIn(echoPin, HIGH);

    // Calculating the distance
    return duration * 0.034 / 2;
}

uint8_t mapDistanceToIntensityValue(double distance) {
    // Scale distance to become a value between 0 and 255
    return static_cast<uint8_t>(MAX_BRIGHTNESS - ((MAX_BRIGHTNESS * distance) / DISTANCE_TRIGGER));
}

void setup() {
    pinMode(TRIG_PIN_ULTRA_ONE, OUTPUT); // Sets the TRIG_PIN as an Output
    pinMode(ECHO_PIN_ULTRA_ONE, INPUT); // Sets the ECHO_PIN as an Input

    pinMode(TRIG_PIN_ULTRA_TWO, OUTPUT); // Sets the TRIG_PIN as an Output
    pinMode(ECHO_PIN_ULTRA_TWO, INPUT); // Sets the ECHO_PIN as an Input

    pinMode(GREEN_LED, OUTPUT); // Sets the GREEN_LED as an Output
    pinMode(RED_LED, OUTPUT); // Sets the RED_LED as an Output

    dmxMaster.enable(); // Enable DMX master interface and start transmitting
}

void loop() {
    // Calculating the distance
    distanceUltraOne = sensorSense(TRIG_PIN_ULTRA_ONE, ECHO_PIN_ULTRA_ONE);
    distanceUltraTwo = sensorSense(TRIG_PIN_ULTRA_TWO, ECHO_PIN_ULTRA_TWO);

    if (distanceUltraOne <= DISTANCE_TRIGGER || distanceUltraTwo <= DISTANCE_TRIGGER) {
        // Set DMX to a mixture of red and blue dependant on proximity to sensor
        redOn();
        uint8_t blueIntensity = mapDistanceToIntensityValue(distanceUltraOne);
        uint8_t redIntensity = mapDistanceToIntensityValue(distanceUltraTwo);
        setRGB(redIntensity, MIN_BRIGHTNESS, blueIntensity);
    } else {
        // Green light on by default
        greenOn();
        setRGB(MIN_BRIGHTNESS, MAX_BRIGHTNESS, MIN_BRIGHTNESS);
    }
}
